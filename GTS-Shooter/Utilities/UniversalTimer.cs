﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTS_Shooter.Utilities
{
    class UniversalTimer
    {
        private double currentTime;
        private double countDuration;
        private int counter;

        private Action action;
        private double delay;
        private bool DoAction;
        private double actionCounter;

        public int Counter
        {
            get { return this.counter; }
        }

        public UniversalTimer(double countDuration)// 1 = one second
        {
            Initialize(countDuration);
        }
        
        public void Initialize(double countDuration)
        {
            this.countDuration = countDuration;
            this.counter = 0;
            this.delay = 0;
            this.DoAction = false;
            this.actionCounter = 0;
        }

        public void Reset()
        {
            counter = 0;
        }

        public void Update(GameTime gameTime)
        {
            this.currentTime += gameTime.ElapsedGameTime.TotalSeconds; //Time passed since last Update() 

            if (this.currentTime >= this.countDuration)
            {
                this.counter++;

                if (this.DoAction)
                {
                    this.actionCounter++;
                }

                this.currentTime -= this.countDuration; // "use up" the time                                          
            }

           

           
        }

       

    }
}


