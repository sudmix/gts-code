﻿//using EmptyKeys.UserInterface;
//using EmptyKeys.UserInterface.Controls;
//using EmptyKeys.UserInterface.Generated;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Shooter;
using System.Collections.Generic;
using System;
using System.Threading;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using GTS_Shooter.Utilities;
using GameLibary;



namespace Shooter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        public GameObjectManager GameObjects;
        public ProcessManager Processes;
        public ThePlayer PlayerOne;
        public Asteroid AsteroidOne;
        public Satellit SatellitOne;

        bool startTimer;

        GraphicsDeviceManager graphics;

        UniversalTimer GameTimer;

        SpriteBatch GamesSpriteBatch;

        LightningBolt LightningBrunch;

        // background
        Texture2D mainBackground;

        // End background
        Texture2D endBackground;

        Texture2D Ship;

        ParallaxingBackground bgLayer1;

        ParallaxingBackground bgLayer2;

        Rectangle rectBackground;
        // enemies
       

      

        TimeSpan enemySpawnTime;

        TimeSpan previousSpawnTime;

        Random random;

     
        TimeSpan laserSpawnTime;

        TimeSpan previousLaserSpawnTime;

     

        private Song gameMusic;

        KeyboardState currentKeyboardState;

        KeyboardState previousKeyboardState;

        GamePadState currentGamePadState;

        GamePadState previousGamePadState;


        SpriteFont TextSprite;

        GameState TheGame;

       

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            // Test
            Processes = new ProcessManager();
            GameObjects = new GameObjectManager();

            Processes.AddProcess(new GravityProcess2D(true));
            Processes.AddProcess(new MoveProcess2D(true));
            Processes.AddProcess(new RenderProcess2D(false));
            Processes.AddProcess(new AnimationProcess2D(true));
            Processes.AddProcess(new CollisionProcess2D(GraphicsDevice, true));
            Processes.AddProcess(new ManualSteeringProcess(true));
            Processes.AddProcess(new ParticleSystemProcess2D(true));

            PlayerOne = new ThePlayer();
            AsteroidOne = new Asteroid();
            PlayerOne.Initialize(Content, Processes, new Vector2(200,200));
            AsteroidOne.Initialize(Content, Processes);
            //////////////////////////////////////////////
            //LightningBrunch = new BranchLightning(new Vector2(100, 100), new Vector2(700, 700));

            // Initialize the player class
            startTimer = false;
            // one second Timer
            GameTimer = new UniversalTimer(1); 

            TheGame = new GameState();

           

            //Background
            bgLayer1 = new ParallaxingBackground();

            bgLayer2 = new ParallaxingBackground();

            rectBackground = new Rectangle(0, 0, GraphicsDevice.Viewport.TitleSafeArea.Width, GraphicsDevice.Viewport.TitleSafeArea.Height);

          

            previousSpawnTime = TimeSpan.Zero;

            enemySpawnTime = TimeSpan.FromSeconds(1.0f);

            random = new Random();

           

            TouchPanel.EnabledGestures = GestureType.FreeDrag;


            const float SECONDS_IN_MINUTE = 60f;

            const float RATE_OF_FIRE = 250f;

            laserSpawnTime = TimeSpan.FromSeconds(SECONDS_IN_MINUTE / RATE_OF_FIRE);

            previousLaserSpawnTime = TimeSpan.Zero;

            

            TheGame.Initialize();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
       //     root = new UIRoot();
            
            // sprite batch for all textures
            GamesSpriteBatch = new SpriteBatch(GraphicsDevice);


            Art.Load(Content);
         

            mainBackground = Content.Load<Texture2D>("Graphics\\mainbackground");

            endBackground = Content.Load<Texture2D>("Graphics\\endMenu");

            Ship = Content.Load<Texture2D>("Graphics//ship"); 

            



            gameMusic = Content.Load<Song>("Sounds\\gameMusic");

            TextSprite = Content.Load<SpriteFont>("Graphics\\gameFont");

            MediaPlayer.Play(gameMusic);
            MediaPlayer.IsRepeating = true;
            // stop music
            // MediaPlayer.Stop();

            bgLayer1.Initialize(Content, "Graphics\\bgLayer1", GraphicsDevice.Viewport.TitleSafeArea.Width, GraphicsDevice.Viewport.TitleSafeArea.Height, -1);

            bgLayer2.Initialize(Content, "Graphics\\bgLayer2", GraphicsDevice.Viewport.TitleSafeArea.Width, GraphicsDevice.Viewport.TitleSafeArea.Height, -2);

         

            Vector2 playerPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X, GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.TitleSafeArea.Height / 2);

 

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        

            AsteroidOne.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
           

            Processes.Update(gameTime);


            
            GameTimer.Update(gameTime);

            if(LightningBrunch != null)
                 LightningBrunch.Update();

                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    Exit();
            if (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.A))
                TheGame.ChangeState(GameState.States.endMenu);



            previousGamePadState = currentGamePadState;

                previousKeyboardState = currentKeyboardState;

                currentKeyboardState = Keyboard.GetState();

                currentGamePadState = GamePad.GetState(PlayerIndex.One);

            if (TheGame.CurrentState == GameState.States.mainMenu)
            {
                // TODO: Mainmenu
                TheGame.ChangeState(GameState.States.gameLoop);
                
            } // Start menu end
            else if (TheGame.CurrentState == GameState.States.gameLoop) // game running
            {
                if (currentKeyboardState.IsKeyDown(Keys.Space) || currentGamePadState.Buttons.X == ButtonState.Pressed)
                {
                    FireLaser(gameTime);

                    
                }

                rectBackground.Height = GraphicsDevice.Viewport.Height;
                rectBackground.Width = GraphicsDevice.Viewport.Width;

                bgLayer1.Update(gameTime, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);

                bgLayer2.Update(gameTime, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);

                UpdateEnemies(gameTime);

               

             

             
            }//game running end
            else if(TheGame.CurrentState == GameState.States.endMenu)
            {
                if(startTimer == false)
                {
                    GameTimer.Reset();
                    startTimer = true;
                }

                if(GameTimer.Counter >= 5)
                {
                   
                    if (GamePad.GetState(PlayerIndex.One).Buttons.BigButton == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Space))
                    {
                        PlayerOne.Reset();
                        TheGame.ChangeState(GameState.States.gameLoop);
                    }

                }
                
               
                        
                

               
            }// End menu end

            base.Update(gameTime);
        }

      
        private void AddEnemy(int Type)
        {
            AsteroidOne = new Asteroid();
            SatellitOne = new Satellit();
                    

            Vector2 position = new Vector2(GraphicsDevice.Viewport.Width + 10 / 2,
            
            // y position between 20  - viewport - 20
            random.Next(20, GraphicsDevice.Viewport.Height - 20));
            

            if(Type == 0)
                AsteroidOne.Initialize(Content, Processes, new Vector2(1200,position.Y));
            else
                SatellitOne.Initialize(Content, Processes, new Vector2(1200, position.Y)); 


        }

        private void UpdateEnemies(GameTime gameTime)      
        {
            // spawn irregular between 0.2 - 1.5 seconds
            enemySpawnTime = TimeSpan.FromSeconds(GetRandomDouble(random,0.6, 1.5f));
            // spawn a new enemy 
            if (gameTime.TotalGameTime - previousSpawnTime > enemySpawnTime)
            {
                previousSpawnTime = gameTime.TotalGameTime;

                Random rand = new Random();
                int Type = 0;

                if (rand.Next(0, 2) == 0)
                    Type = 0;
                else
                    Type = 1;

                AddEnemy(Type);
            }

          

        }
        

        protected void FireLaser(GameTime gameTime)
        {
          
            // govern the rate of fire for our lasers
            if (gameTime.TotalGameTime - previousLaserSpawnTime > laserSpawnTime)
            {
                previousLaserSpawnTime = gameTime.TotalGameTime;

                AddLaser();
                LightningBrunch = new LightningBolt(new Vector2(0, 0), new Vector2(800, 500));


            }
        }

        protected void AddLaser()
        {
            ShipLaser TheLaser = new ShipLaser();
            VectorComponent2D PlayerPosition = (VectorComponent2D) PlayerOne.GameObject.GetComponent("Position2D");
            Vector2 Position = new Vector2();
            // Offset
            Position.X = PlayerPosition.Value.X + 62;
            Position.Y = PlayerPosition.Value.Y + 22;

            TheLaser.GameObject.Parent = PlayerOne.GameObject;

            // cost for shot
            ((IntComponent)PlayerOne.GameObject.GetComponent("Points")).Value -= 5;

            TheLaser.Initialize(Content, Processes, Position);
                
        }

        double GetRandomDouble(Random random, double min, double max)
        {
            return min + (random.NextDouble() * (max - min));
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // Start drawing
            GamesSpriteBatch.Begin();

            if (TheGame.CurrentState == GameState.States.mainMenu)
            {
                

            } // Start menu end
            else if (TheGame.CurrentState == GameState.States.gameLoop) // game running
            {
                
                // draw background
                GamesSpriteBatch.Draw(mainBackground, rectBackground, Color.White);

                bgLayer1.Draw(GamesSpriteBatch);

                bgLayer2.Draw(GamesSpriteBatch);


                IntComponent Ships = (IntComponent)PlayerOne.GameObject.GetComponent("Ships");
                IntComponent Health = (IntComponent)PlayerOne.GameObject.GetComponent("Health");
                IntComponent Points = (IntComponent)PlayerOne.GameObject.GetComponent("Points");

                if (Ships.Value >0)
                {
                    for(int i=0; i<Ships.Value;i++)
                    {
                        GamesSpriteBatch.Draw(Ship, new Rectangle(500 +(i * Ship.Width), 20, Ship.Width , Ship.Height), Color.White);
                    }
                    
                }
                else
                {
                    
                    if(Health.Value <=0)
                    {
                        TheGame.ChangeState(GameState.States.endMenu);
                    }

                }
                 

                

             

                Processes.Render(GamesSpriteBatch);

                
                

                if (Points != null)
                     GamesSpriteBatch.DrawString(TextSprite,"Punkte: " + Points.Value.ToString(), new Vector2(10,50), Color.White);
                GamesSpriteBatch.DrawString(TextSprite, "Zeit: " + ((int)gameTime.TotalGameTime.TotalMinutes).ToString() + ":" + ((int)gameTime.TotalGameTime.TotalSeconds).ToString(), new Vector2(300, 0), Color.White);
                GamesSpriteBatch.DrawString(TextSprite, "Gesundheit: " + Health.Value.ToString(), new Vector2(800, 0), Color.White);
            }//game running end
            else if (TheGame.CurrentState == GameState.States.endMenu)
            {
                // draw background
                GamesSpriteBatch.Draw(endBackground, rectBackground, Color.White);
                IntComponent Points = (IntComponent)PlayerOne.GameObject.GetComponent("Points");

                GamesSpriteBatch.DrawString(TextSprite, "Punkte: " + Points.Value.ToString(), new Vector2(300, 500), Color.White);

                if(GameTimer.Counter < 5)
                     GamesSpriteBatch.DrawString(TextSprite, "Start in 5 Sek: " + GameTimer.Counter.ToString(), new Vector2(300, 550), Color.White);
                else
                    GamesSpriteBatch.DrawString(TextSprite, "press space ... ", new Vector2(300, 550), Color.White);


            }// End menu end

            // Stop drawing
            GamesSpriteBatch.End();

            GamesSpriteBatch.Begin(SpriteSortMode.Texture, BlendState.Additive);

            if (LightningBrunch != null)
                LightningBrunch.Draw(GamesSpriteBatch);

            GamesSpriteBatch.End();

            base.Draw(gameTime);
        }


    }
}
