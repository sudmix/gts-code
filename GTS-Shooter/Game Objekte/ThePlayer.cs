﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


namespace Shooter
{
    public class ThePlayer 
    {
        public ThePlayer()
        {
            GameObject = new GameLibary.GameObject("Player");
        }

        public void Initialize(ContentManager Content, ProcessManager Processes, Vector2 StartPosition)
        {
            PlayerStartPosition = StartPosition;
            // create components
            VectorComponent2D Position = new VectorComponent2D(GameObject, "Position2D", StartPosition);
            VectorComponent2D Velocity = new VectorComponent2D(GameObject, "Velocity2D", 15, 15);
            VectorComponent2D Gravity = new VectorComponent2D(GameObject, "Gravity2D", 0, 150);
            RenderComponent2D Renderer = new RenderComponent2D(GameObject, Content, "Graphics//ship");
            AnimationComponent2D Animation = new AnimationComponent2D(GameObject, Content, "Graphics\\PlayerAnimation");
            CollisionComponentRect Collision = new CollisionComponentRect("PlayerRect",GameObject,0,120,0, 60);
            PlayerCollider ThePlayerCollider = new PlayerCollider(GameObject);
            IntComponent Health = new IntComponent(GameObject, "Health");
            IntComponent Damage = new IntComponent(GameObject, "Damage");
            IntComponent Points = new IntComponent(GameObject, "Points");
            IntComponent Ships = new IntComponent(GameObject, "Ships");

            SoundComponent Explosion = new SoundComponent(GameObject, Content, "Sounds\\explosion");

            Health.Value = 100;
            Damage.Value = 10;
            Points.Value = 0;
            Ships.Value = 3;

            ThePlayerCollider.OwnerName = "Player";
      
            ThePlayerCollider.AddIgnoreRect("ShipLaser");

            ParticleComponent2D particleSystem = new ParticleComponent2D(GameObject, Content, "Graphics/Particle", Position.Value);

            particleSystem.component.AddEmitter(new Vector2(0.0001f, 0.0015f),
                                       new Vector2(-1, -0.5f), new Vector2(0.1f * MathHelper.Pi, 0.01f * -MathHelper.Pi),
                                       new Vector2(1.0f, 3.05f),
                                       new Vector2(4, 4), new Vector2(20, 50f),
                                       Color.Wheat, Color.Gray, new Color(Color.Black, 0), new Color(Color.Black, 0),
                                       new Vector2(60, 40), new Vector2(20, 20), 3000, Vector2.Zero, particleSystem.Texture);

            particleSystem.component.AddEmitter(new Vector2(0.01f, 0.0015f),
                                        new Vector2(-1, -0.9f), new Vector2(0.2f * MathHelper.Pi, 0.01f * -MathHelper.Pi),
                                        new Vector2(1.0f, 2.05f),
                                        new Vector2(5, 5), new Vector2(20, 40f),
                                        Color.Gray, Color.Wheat, new Color(Color.Black, 0), new Color(Color.Black, 0),
                                        new Vector2(60, 30), new Vector2(20, 20), 3000, Vector2.Zero, particleSystem.Texture);


            GameObject.AddComponent(Position);
            GameObject.AddComponent(Velocity);
            GameObject.AddComponent(Gravity);
            GameObject.AddComponent(Renderer);
            GameObject.AddComponent(Animation);
            GameObject.AddComponent(Collision);
            GameObject.AddComponent(Health);
            GameObject.AddComponent(Damage);
            GameObject.AddComponent(Points);
            GameObject.AddComponent(Explosion);
            GameObject.AddComponent(Ships);
            GameObject.AddComponent(particleSystem);

            // create ProcessNodes
            Processes.GetProcess("GravityProcess2D").Add(new GravityNode2D("PlayerGravity",Position,true, Gravity, false));
            Processes.GetProcess("RenderProcess2D").Add(new RenderNode2D("PlayerRender", Renderer, Position));
            Processes.GetProcess("AnimationProcess2D").Add(new AnimationNode2D("PlayerAnimation", Animation,true, Position, 120, 60, 9, 60, 1f, true));
            Processes.GetProcess("CollisionProcess2D").Add(new ColliderNode2D("PlayerCollider", Collision, Position, ThePlayerCollider));
            Processes.GetProcess("ManualSteeringProcess").Add(new ManualSteeringNode("PlayerPosition",Position, Velocity));
            Processes.GetProcess("ParticleSystemProcess2D").Add(new ParticleNode2D("PlayerParticle", particleSystem, Position, false));


        }
        public void Unload()
        {
            
        }

        public void Reset()
        {
            ((VectorComponent2D)GameObject.GetComponent("Gravity2D")).IsActive = false;
            ((VectorComponent2D)GameObject.GetComponent("Position2D")).Value = PlayerStartPosition;
            ((IntComponent)GameObject.GetComponent("Health")).Value = 100;
            ((IntComponent)GameObject.GetComponent("Points")).Value = 0;
            ((IntComponent)GameObject.GetComponent("Ships")).Value = 3;
           
        }

        public GameLibary.GameObject GameObject;
        Vector2 PlayerStartPosition;

    }
}
