﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Shooter
{
    public class ShipLaser
    {
        public ShipLaser()
        {
            GameObject = new GameLibary.GameObject("ShipLaser");
        }

        public void Initialize(ContentManager Content, ProcessManager Processes, Vector2 StartPosition)
        {
          
            // create components
            VectorComponent2D Position = new VectorComponent2D(GameObject, "Position2D", StartPosition);
            VectorComponent2D Velocity = new VectorComponent2D(GameObject, "Velocity2D", 1200, 0);
            AnimationComponent2D Animation = new AnimationComponent2D(GameObject, Content, "Graphics\\laser");
            CollisionComponentRect Collision = new CollisionComponentRect("LaserRect", GameObject, 0, 46, 0, 16);
            LaserCollider TheLaserCollider = new LaserCollider(GameObject);
            IntComponent Damage = new IntComponent(GameObject, "Damage");
            SoundComponent Fire = new SoundComponent(GameObject, Content, "Sounds\\laserFire");
            Damage.Value = 10;

            TheLaserCollider.OwnerName = "ShipLaser";
            TheLaserCollider.AddIgnoreRect("Player");


            GameObject.AddComponent(Position);
            GameObject.AddComponent(Velocity);
            GameObject.AddComponent(Animation);
            GameObject.AddComponent(Collision);
            GameObject.AddComponent(Damage);
            GameObject.AddComponent(Fire);
       


            // create ProcessNodes
            Processes.GetProcess("MoveProcess2D").Add(new MoveNode2D("LaserMove",Position, true, Velocity, true));
            Processes.GetProcess("AnimationProcess2D").Add(new AnimationNode2D("LaserAnimation",Animation,true, Position, 46, 16, 1, 30, 1f, true));
            Processes.GetProcess("CollisionProcess2D").Add(new ColliderNode2D("LaserCollider",Collision, Position, TheLaserCollider));

            Fire.SoundInstance.Play();

        }

        public void Unload()
        {
            SoundComponent SoundComp = (SoundComponent)GameObject.GetComponent("Sounds\\laserFire");
            SoundComp.SoundInstance.Dispose();
        }



        public GameLibary.GameObject GameObject;


    }
}
