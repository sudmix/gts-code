﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Shooter
{
    public class Satellit
    {
        public Satellit()
        {
            GameObject = new GameLibary.GameObject("Satellit");
        }

        public void Initialize(ContentManager Content, ProcessManager Processes)
        {
            Initialize(Content, Processes, new Vector2(1200, 500));
        }
        public void Initialize(ContentManager Content, ProcessManager Processes, Vector2 StartPosition)
        {
            // create components
            VectorComponent2D Position = new VectorComponent2D(GameObject, "Position2D", (int)StartPosition.X, (int)StartPosition.Y);
            VectorComponent2D Velocity = new VectorComponent2D(GameObject, "Velocity2D", -350, 0);
            VectorComponent2D Gravity = new VectorComponent2D(GameObject, "Gravity2D", 0, 350);
            RenderComponent2D Renderer = new RenderComponent2D(GameObject, Content, "Graphics//ship");
            AnimationComponent2D Animation = new AnimationComponent2D(GameObject, Content, "Graphics\\SatAnim");
            AnimationComponent2D ExplosionAnimation = new AnimationComponent2D(GameObject, Content, "Graphics\\explosion");
            CollisionComponentRect Collision = new CollisionComponentRect("SatellitRect", GameObject, 0, 80, 0, 53);
            SatellitCollider TheSatellitCollider = new SatellitCollider(GameObject);
            IntComponent Health = new IntComponent(GameObject, "Health");
            IntComponent Damage = new IntComponent(GameObject, "Damage");
            IntComponent GivePoints = new IntComponent(GameObject, "GivePoints");
            SoundComponent Explosion = new SoundComponent(GameObject, Content, "Sounds\\explosion");

            Health.Value = 10;
            Damage.Value = 10;
            GivePoints.Value = 15;

            TheSatellitCollider.OwnerName = "Satellit";


            GameObject.AddComponent(Position);
            GameObject.AddComponent(Velocity);
            GameObject.AddComponent(Gravity);
            GameObject.AddComponent(Renderer);
            GameObject.AddComponent(Animation);
            GameObject.AddComponent(ExplosionAnimation);
            GameObject.AddComponent(Collision);
            GameObject.AddComponent(Health);
            GameObject.AddComponent(Damage);
            GameObject.AddComponent(Explosion);
            GameObject.AddComponent(GivePoints);
            

            Position.IsActive = true;
            Velocity.IsActive = true;

            // create ProcessNodes
            Processes.GetProcess("MoveProcess2D").Add(new MoveNode2D("AsteroidMove", Position, true, Velocity, true));
            Processes.GetProcess("GravityProcess2D").Add(new GravityNode2D("AsteroidGravity", Position, true, Gravity, false));
            Processes.GetProcess("RenderProcess2D").Add(new RenderNode2D("AsteroidRender", Renderer, Position));
            Processes.GetProcess("AnimationProcess2D").Add(new AnimationNode2D("AsteroidAnimation", Animation, true, Position, 80, 53, 22, 200, 1f, true));
            Processes.GetProcess("AnimationProcess2D").Add(new AnimationNode2D("AsteroidExplosion", ExplosionAnimation, false, Position, 134, 134, 12, 30, 1f, false));
            Processes.GetProcess("CollisionProcess2D").Add(new ColliderNode2D("SatellitCollider", Collision, Position, TheSatellitCollider));


        }

        public void Unload()
        {
            SoundComponent SoundComp = (SoundComponent)GameObject.GetComponent("Sounds\\explosion");
            SoundComp.SoundInstance.Dispose();
        }



        GameLibary.GameObject GameObject;
    }
}
