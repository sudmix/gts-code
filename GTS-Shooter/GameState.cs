﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shooter;
using Windows.Graphics.Display;


namespace Shooter
{
    class GameState
    {
        public enum States
        {
            mainMenu = 0,
            gameLoop = 1,
            endMenu  = 2
        };

        private States state;

        public States CurrentState
        {
            get { return this.state; }
        }

        public void Initialize()
        {
            this.state = States.mainMenu;
        }

        public void ChangeState(States CurrentState)
        {
            this.state = CurrentState;
        }

       
    }
}
