﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameLibary
{
    public class AnimationNode2D : IProcessNode
    {
        public AnimationNode2D(string Name,IComponent Animation2D, bool ActivateAnimation, IComponent Position2D, int frameWidth, int frameHeight, int frameCount, int frametime, float scale, bool looping)
        {
            this.Position = (VectorComponent2D)Position2D;
            this.Animation = (AnimationComponent2D)Animation2D;
            this.Animation.IsActive = ActivateAnimation;
            
            Animation.Animator.Initialize(Animation.Texture, Position, frameWidth, frameHeight, frameCount, frametime, Color.White, scale, looping);

            this.Type = Name;
        }

        public string Type
        {
            get
            {
                return Typename;
            }

            set
            {
                Typename = value;
            }
        }

        public bool Equals(IProcessNode other)
        {
            if (other == null) return false;
            return (this.GetType().Equals(other.GetType()));
        }

        string Typename;
        public VectorComponent2D Position;
        public AnimationComponent2D Animation;
    }
}
