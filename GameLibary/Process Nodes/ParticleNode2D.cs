﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class ParticleNode2D : IProcessNode
    {
        public ParticleNode2D(string Name, IComponent ParticleSys, IComponent Position, bool ActivateParticle)
        {
            this.Position = (VectorComponent2D)Position;
            this.particleSystem = (ParticleComponent2D)ParticleSys;
            this.particleSystem.IsActive = ActivateParticle;
            Typename = Name;
        }

        public string Type
        {
            get
            {
                return Typename;
            }

            set
            {
                Typename = value;
            }
        }

        public bool Equals(IProcessNode other)
        {
            if (other == null) return false;
            return (this.GetType().Equals(other.GetType()));
        }

        public ParticleComponent2D particleSystem;
        public VectorComponent2D Position;
        private string Typename;
    }
}
