﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class GravityNode2D : IProcessNode
    {
        public GravityNode2D(string Name, IComponent Position,bool ActivatePosition, IComponent Gravity, bool ActivateGravity)
        {
            this.Position = ((VectorComponent2D)Position);
            this.Gravity = ((VectorComponent2D)Gravity);
            this.Position.IsActive = ActivatePosition;
            this.Gravity.IsActive = ActivateGravity;
            this.Type = Name;
        }
        public string Type
        {
            get
            {
                return Typename;
            }

            set
            {
                Typename = value;
            }
        }

        public bool Equals(IProcessNode other)
        {
            if (other == null) return false;
            return (this.GetType().Equals(other.GetType()));
        }

        private string Typename;
        public VectorComponent2D Position;
        public VectorComponent2D Gravity;
    }
}
