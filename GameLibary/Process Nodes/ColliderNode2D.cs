﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class ColliderNode2D : IProcessNode
    {
        public ColliderNode2D(string Name,CollisionComponentRect Rect, VectorComponent2D Position,  ICollider Collider)
        {
            TheRect = Rect;    
            TheCollider = Collider;
            ThePosition = Position;
            Typename = Name;
        }

        public string Type
        {
            get
            {
                return Typename;
            }

            set
            {
                Typename = value;
            }
        }

        public bool Equals(IProcessNode other)
        {
            if (other == null) return false;
            return (this.Type.Equals(other.Type));
        }

        private string Typename;
        public CollisionComponentRect TheRect;
        public ICollider TheCollider;
        public VectorComponent2D ThePosition;
    }
}
