﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class MoveNode2D : IProcessNode
    {
        public MoveNode2D(string Name, IComponent Position, bool ActivatePosition, IComponent Velocity, bool ActivateVelocity)
        {
            this.Position = ((VectorComponent2D)Position);
            this.Velocity = ((VectorComponent2D)Velocity);
            this.Position.IsActive = ActivatePosition;
            this.Velocity.IsActive = ActivateVelocity;
            this.Type = Name;
        }

        string IProcessNode.Type
        {
            get
            {
                return this.Type;
            }

            set
            {
                this.Type = value;
            }
        }

        bool IEquatable<IProcessNode>.Equals(IProcessNode other)
        {
            if (other == null) return false;
            return (this.GetType().Equals(other.GetType()));
        }

        private string Type;
        public VectorComponent2D Position;
        public VectorComponent2D Velocity;
    }
}
