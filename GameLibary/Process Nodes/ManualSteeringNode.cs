﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class ManualSteeringNode : IProcessNode
    {
        public ManualSteeringNode(string Name, IComponent Position, IComponent Velocity)
        {
            Typename = Name;

            this.Position = (VectorComponent2D)Position;

            this.Velocity = (VectorComponent2D)Velocity;

            
    }

        public string Type
        {
            get
            {
                return Typename;
            }

            set
            {
                Typename = value;
            }
        }

        public bool Equals(IProcessNode other)
        {
            if (other == null) return false;
            return (this.GetType().Equals(other.GetType()));
        }

        
        private string Typename;
        public KeyboardState TheCurrentKeyboardState;

        public KeyboardState ThePreviousKeyboardState;

        public GamePadState TheCurrentGamePadState;

        public GamePadState ThePreviousGamePadState;

        public VectorComponent2D Position;

        public VectorComponent2D Velocity;

    }
}
