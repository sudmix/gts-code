﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class AsteroidCollider : ICollider
    {
        public AsteroidCollider(Entity Owner)
        {
            TheOwner = Owner;
            Trigger = false;
            Info = new ColliderInfo();
            IgnoreList = new List<string>();
        }

        public AsteroidCollider()
        {
            Trigger = false;
            Info = new ColliderInfo();
            IgnoreList = new List<string>();
        }

        public string OwnerName
        {
            set { NameOwner = value; }
            get { return NameOwner; }
        }

        public void AddIgnoreRect(string rect)
        {
            IgnoreList.Add(rect);
        }

        public bool DoIgnore(string rect)
        {
            foreach (string tag in IgnoreList)
            {
                if (tag.Equals(rect))
                    return true;
            }

            return false;
        }
        public bool IsTrigger
        {
            get
            {
                return Trigger;
            }

            set
            {
                Trigger = value;
            }
        }

        public Entity TheOwner
        {
            get
            {
                return Owner;
            }

            set
            {
                Owner = value;
            }
        }

        public ColliderInfo CollInfo
        {
            get
            {
                return Info;
            }

            set
            {
                Info = value;
            }
        }

        public void OnEnterTrigger(Entity Other)
        {



            Info.Other = Other;
            Info.EnteredCollider = true;
            Info.IsCollide = true;
            Info.ExitCollider = false;
        }

        public void OnExitTrigger(Entity Other)
        {
            if (Owner.GetComponent("Graphics\\AsteroidAnimGlow") != null) 
                 Owner.GetComponent("Graphics\\AsteroidAnimGlow").IsActive = true;
            if(Owner.GetComponent("Graphics\\AsteroidAnim") != null)
                Owner.GetComponent("Graphics\\AsteroidAnim").IsActive = false;

            Owner.GetComponent("Gravity2D").IsActive = true;

           
            IntComponent Health = (IntComponent)Owner.GetComponent("Health");
            IntComponent Damage = (IntComponent)Other.GetComponent("Damage");

           


            if ((Health != null) && (Damage != null))
            {
                Health.Value -= Damage.Value;

                if (Health.Value <= 0)
                {
                    // Geschwindigkeit auf false setzen
                    Owner.GetComponent("Graphics\\AsteroidAnim").IsActive = false;
                    Owner.GetComponent("Graphics\\AsteroidAnimGlow").IsActive = false;
                    
                    Owner.GetComponent("AsteroidRect").IsActive = false;
                    Owner.GetComponent("Graphics\\explosion").IsActive = true;
                    SoundComponent Explosion =(SoundComponent) Owner.GetComponent("Sounds\\explosion");
                    Explosion.SoundInstance.Play();
                }
            }
                

            

            Info.EnteredCollider = false;
            Info.IsCollide = false;
            Info.ExitCollider = true;
            
            Owner.IsActive = false;
        }

        bool Trigger;
        Entity Owner;
        string NameOwner;
        ColliderInfo Info;
        List<string> IgnoreList;
    }
}
