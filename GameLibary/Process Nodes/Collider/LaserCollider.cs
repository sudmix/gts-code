﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class LaserCollider : ICollider
    {
        public LaserCollider(Entity Owner)
        {
            TheOwner = Owner;
            Trigger = false;
            Info = new ColliderInfo();
            IgnoreList = new List<string>();
        }

        public LaserCollider()
        {
            Trigger = false;
            Info = new ColliderInfo();
            IgnoreList = new List<string>();
        }
        public string OwnerName
        {
            set { NameOwner = value; }
            get { return NameOwner; }
        }


        public void AddIgnoreRect(string rect)
        {
            IgnoreList.Add(rect);
        }

        public bool DoIgnore(string rect)
        {
            foreach (string tag in IgnoreList)
            {
                if (tag.Equals(rect))
                    return true;
            }

            return false;
        }

        public bool IsTrigger
        {
            get
            {
                return Trigger;
            }

            set
            {
                Trigger = value;
            }
        }

        public Entity TheOwner
        {
            get
            {
                return Owner;
            }

            set
            {
                Owner = value;
            }
        }

        public ColliderInfo CollInfo
        {
            get
            {
                return Info;
            }

            set
            {
                Info = value;
            }
        }

        public void OnEnterTrigger(Entity Other)
        {




            Info.Other = Other;
            Info.EnteredCollider = true;
            Info.IsCollide = true;
            Info.ExitCollider = false;
        }

        public void OnExitTrigger(Entity Other)
        {
            Owner.GetComponent("Graphics\\laser").IsActive = false;
            Owner.GetComponent("LaserRect").IsActive = false;


            IntComponent Health = (IntComponent)Owner.GetComponent("Health");
            IntComponent Damage = (IntComponent)Other.GetComponent("Damage");
            IntComponent GivePoints = (IntComponent)Other.GetComponent("GivePoints");

            if ((Health != null) && (Damage != null))
                 Health.Value -= Damage.Value;

            Entity Parent = Owner.Parent;

            if (Parent != null)
            {
                IntComponent Points = (IntComponent)Parent.GetComponent("Points");

                if ((Points != null)&&(GivePoints != null))
                    Points.Value += GivePoints.Value;

            }

            Info.EnteredCollider = false;
            Info.IsCollide = false;
            Info.ExitCollider = true;
     
            Owner.IsActive = false;
        }

        bool Trigger;
        Entity Owner;
        string NameOwner;
        public ColliderInfo Info;
        List<string> IgnoreList;
    }
}
