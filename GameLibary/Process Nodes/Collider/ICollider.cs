﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public interface ICollider
    {
        void OnEnterTrigger(Entity Other);
        void OnExitTrigger(Entity Other);

        void AddIgnoreRect(string rect);

        bool DoIgnore(string rect);

        string OwnerName { get; set; }

        bool IsTrigger {get; set;}

        Entity TheOwner { get; set; } 

        ColliderInfo CollInfo { get; set; }
    }
}
