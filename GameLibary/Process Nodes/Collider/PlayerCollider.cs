﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class PlayerCollider : ICollider
    {
        public PlayerCollider(Entity Owner)
        {
            TheOwner = Owner;
            Trigger = false;
            Info = new ColliderInfo();
            IgnoreList = new List<string>();

          
        }

        public PlayerCollider()
        {
            Info = new ColliderInfo();
            IgnoreList = new List<string>();
        }
        public string OwnerName
        {
            set { NameOwner = value; }
            get { return NameOwner; }
        }


        public void AddIgnoreRect(string rect)
        {
            IgnoreList.Add(rect);
        }

        public bool DoIgnore(string rect)
        {
            foreach(string tag in IgnoreList)
            {
                if (tag.Equals(rect))
                    return true;
            }

            return false;
        }

        public bool IsTrigger
        {
            get
            {
                return Trigger;
            }

            set
            {
                Trigger = value;
            }
        }

        public Entity TheOwner
        {
            get
            {
                return Owner;
            }

            set
            {
                Owner = value;
            }
        }

        public ColliderInfo CollInfo
        {
            get
            {
                return Info;
            }

            set
            {
                Info = value;
            }
        }

        public void OnEnterTrigger(Entity Other)
        {           
            // Geschwindigkeit auf false setzen
            Owner.GetComponent("Velocity2D").IsActive = true;
       //     Owner.GetComponent("Gravity2D").IsActive = false;
            Info.EnteredCollider = true;
            Info.IsCollide = true;
            Info.ExitCollider = false;
            Info.Other = Other;
        }

        public void OnExitTrigger(Entity Other)
        {


            IntComponent Health = (IntComponent)Owner.GetComponent("Health");
            VectorComponent2D Gravity = (VectorComponent2D)Owner.GetComponent("Gravity2D");
            ParticleComponent2D particle = (ParticleComponent2D)Owner.GetComponent("ParticleComponent2D");
            IntComponent Damage = (IntComponent)Other.GetComponent("Damage");

            if ((Health != null) && (Damage != null))
            {
                Health.Value -= Damage.Value;

                // damaged ship, drop down
                if(Health.Value < 50)
                {
                    Gravity.IsActive = true;
                    particle.IsActive = true;
                }
                else
                {
                    Gravity.IsActive = false;
                    particle.IsActive = false;
                }

                if(Health.Value <=0)
                {
                    IntComponent Ships = (IntComponent)Owner.GetComponent("Ships");

                    if(Ships.Value > 0)
                    {
                        Health.Value = 100;
                        Ships.Value -= 1;
                    }
                }
            }



            
            Info.EnteredCollider = false;
            Info.IsCollide = false;
            Info.ExitCollider = true;
        }

        bool Trigger;
        Entity Owner;
        string NameOwner;
        ColliderInfo Info;
        List<string> IgnoreList;
    }
}
