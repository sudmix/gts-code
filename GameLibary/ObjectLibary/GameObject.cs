﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameLibary;

namespace GameLibary
{
    public class GameObject : Entity
    {
        public GameObject(string Name) : base(Name)
        {
            
            Tag = Name;
        }
        public string TheName
        {
            get
            {
                return Tag;
            }

            set
            {
                Tag = value;
            }
        }

        string Tag;

        
    }
}
