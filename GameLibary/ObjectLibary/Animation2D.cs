﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class Animation2D
    {
        #region private member
        // member variables
        Texture2D spriteStrip;

        float spriteScale;

        int elapsedTime;

        int frameTime;

        int frameCount;

        int currentFrame;

        Color spriteColor;

        Rectangle spriteSourceRect = new Rectangle();

        Rectangle spriteDestinationRect = new Rectangle();
        #endregion

        #region public member
        public int frameWidth;

        public int frameHeight;

        public bool active;

        public bool looping;

        public VectorComponent2D Position;

        #endregion

        #region public functions
        public void Initialize(Texture2D texture, VectorComponent2D Position, int frameWidth, int frameHeight, int frameCount, int frametime, Color color, float scale, bool looping)

        {
            this.spriteColor = color;
            this.frameWidth = frameWidth;
            this.frameHeight = frameHeight;
            this.frameCount = frameCount;
            this.frameTime = frametime;
            this.spriteScale = scale;
            this.looping = looping;
            this.Position = Position;
            this.spriteStrip = texture;
            this.elapsedTime = 0;
            this.currentFrame = 0;
            this.active = true;

        }

        public void Update(GameTime gameTime)

        {
            // update only active animation
            if (active == false) return;

            this.elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;

            // frame update
            if (this.elapsedTime > frameTime)

            {
                this.currentFrame++;

                if (this.currentFrame == frameCount)
                {
                    this.currentFrame = 0;

                    // no loop 
                    if (this.looping == false)
                        this.active = false;

                }

                this.elapsedTime = 0;

            }
            // compute rects
            this.spriteSourceRect = new Rectangle(currentFrame * frameWidth, 0, frameWidth, frameHeight);

            this.spriteDestinationRect = new Rectangle((int)Position.Value.X - (int)(frameWidth * spriteScale) / 2,

            (int)Position.Value.Y - (int)(frameHeight * spriteScale) / 2,

            (int)(frameWidth * spriteScale),

            (int)(frameHeight * spriteScale));

        }



        public void Draw(SpriteBatch spriteBatch)

        {
            // draw only active animation
            if (active == false) return;

            spriteBatch.Draw(spriteStrip, spriteDestinationRect, spriteSourceRect, spriteColor);


        }

        #endregion
    }
}
