﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class DrawRectangle
    {
       

            #region private member
            private Texture2D pixel;

            private Rectangle rectangleToDraw;

            #endregion

            #region public methods
            public void Initialize(GraphicsDevice gfxdevice)
            {
                // Somewhere in your LoadContent() method:
                pixel = new Texture2D(gfxdevice, 1, 1, false, SurfaceFormat.Color);
                pixel.SetData(new[] { Color.White }); // so that we can draw whatever color we want on top of it
            }



            public void Update(Rectangle _rectangleToDraw)
            {
                rectangleToDraw = _rectangleToDraw;
            }



            public void Draw(SpriteBatch spriteBatch, int thicknessOfBorder, Color borderColor)
            {

                // Draw top line
                spriteBatch.Draw(pixel, new Rectangle(rectangleToDraw.X, rectangleToDraw.Y, rectangleToDraw.Width, thicknessOfBorder), borderColor);

                // Draw left line
                spriteBatch.Draw(pixel, new Rectangle(rectangleToDraw.X, rectangleToDraw.Y, thicknessOfBorder, rectangleToDraw.Height), borderColor);

                // Draw right line
                spriteBatch.Draw(pixel, new Rectangle((rectangleToDraw.X + rectangleToDraw.Width - thicknessOfBorder),
                                                rectangleToDraw.Y,
                                                thicknessOfBorder,
                                                rectangleToDraw.Height), borderColor);
                // Draw bottom line
                spriteBatch.Draw(pixel, new Rectangle(rectangleToDraw.X,
                                                rectangleToDraw.Y + rectangleToDraw.Height - thicknessOfBorder,
                                                rectangleToDraw.Width,
                                                thicknessOfBorder), borderColor);


            }
            #endregion
        
    }
}
