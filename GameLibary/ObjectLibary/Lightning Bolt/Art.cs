﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public static class Art
    {
        public static Texture2D LightningSegment, HalfCircle, Pixel;

        public static void Load(ContentManager content)
        {
            LightningSegment = content.Load<Texture2D>("Graphics/Lightning Segment");
            HalfCircle = content.Load<Texture2D>("Graphics/Half Circle");
            Pixel = content.Load<Texture2D>("Graphics/Pixel");
        }
    }
}
