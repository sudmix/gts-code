﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class GameObjectManager
    {
        public GameObjectManager()
        {
            GameObjectList = new List<GameObject>();
        }


        public void Add(GameObject GO)
        {
            GameObjectList.Add(GO);
        }

        public bool Remove(GameObject GO)
        {
            return GameObjectList.Remove(GO);
        }

        public GameObject Get(string Tag)
        {
            if (GameObjectList.Exists(item => item.TheName == Tag))
            {
                return GameObjectList.Find(item => item.TheName == Tag);
            }

            return null;
        }

        #region private member
        List<GameObject> GameObjectList;
        #endregion
    }
}
