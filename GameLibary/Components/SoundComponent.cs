﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class SoundComponent : IComponent
    {
        public SoundComponent(Entity Owner, ContentManager Content, string Type)
        {
            Sound = Content.Load<SoundEffect>(Type);

            SoundInstance = Sound.CreateInstance();

            ComponentOwner = Owner;
            Typename = Type;
           
        }

        public string Type
        {
            get
            {
                return Typename;
            }

            set
            {
                Typename = value;
            }
        }

        public Entity Owner
        {
            get
            {
                return ComponentOwner;
            }
        }

        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                Active = value;
            }
        }
        public bool Equals(IComponent other)
        {
            throw new NotImplementedException();
        }

        private string Typename;
        public int Value;
        private Entity ComponentOwner;
        private bool Active;
        private SoundEffect Sound;
        public SoundEffectInstance SoundInstance;
    }
}
