﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class VectorComponent2D : IComponent
    {
        

        public string Type
        {
            get
            {
                return this.Typename;
            }

            set
            {
                this.Typename = value;
            }
        }

        public VectorComponent2D(Entity Owner, string Type)
        {
            ComponentOwner = Owner;
            Typename = Type;
        }

        public VectorComponent2D(Entity Owner, string Type, Vector2 Value)
        {
            ComponentOwner = Owner;
            Typename = Type;
            this.Value = Value;
        }

        public VectorComponent2D(Entity Owner, string Type, int X, int Y)
        {
            ComponentOwner = Owner;
            Typename = Type;
            this.Value = new Vector2(X, Y);
        }

        public Entity Owner
        {
            get
            {
                return ComponentOwner;
            }
        }

        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                Active = value;
            }
        }
        public bool Equals(IComponent other)
        {
            if (other == null) return false;
            return (this.Type.Equals(other.Type));
        }

        private string Typename;
        private Entity ComponentOwner;
        public Vector2 Value;
        private bool Active;

    }
}
