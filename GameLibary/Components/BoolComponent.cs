﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class BoolComponent : IComponent
    {
        public BoolComponent(Entity Owner, string Type)
        {
            ComponentOwner = Owner;
            Typename = Type;
            Value = false;
        }

        public BoolComponent(Entity Owner, string Type, bool Value)
        {
            ComponentOwner = Owner;
            this.Typename = Type;
            this.Value = Value;
        }

        public string Type
        {
            get
            {
                return Typename;
            }

            set
            {
                Typename = value;
            }
        }

        public Entity Owner
        {
            get
            {
                return ComponentOwner;
            }
        }

        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                Active = value;
            }
        }
        public bool Equals(IComponent other)
        {
            throw new NotImplementedException();
        }

        private string Typename;
        public bool Value;
        private Entity ComponentOwner;
        private bool Active;

    }
}
