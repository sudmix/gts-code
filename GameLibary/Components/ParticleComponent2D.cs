﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    public class ParticleComponent2D : IComponent
    {
        public ParticleComponent2D(Entity Owner, ContentManager Content, string Name, Vector2 Position)
        {
            ComponentOwner = Owner;
            this.Texture = Content.Load<Texture2D>(Name);
            this.Type = "ParticleComponent2D";
            component = new ParticleSystem(Position);
        }
        public string Type
        {
            get
            {
                return this.Typename;
            }

            set
            {
                this.Typename = value;
            }
        }

        public Entity Owner
        {
            get
            {
                return ComponentOwner;
            }
        }

        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                Active = value;
            }
        }

        public bool Equals(IComponent other)
        {
            if (other == null) return false;
            return (this.Type.Equals(other.Type));
        }

        public ParticleSystem component;
        private string Typename;
        public Texture2D Texture;
        private Entity ComponentOwner;
        private bool Active;
    }
}
