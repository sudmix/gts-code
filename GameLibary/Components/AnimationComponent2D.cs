﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameLibary
{
    public class AnimationComponent2D : IComponent
    {
        public AnimationComponent2D(Entity Owner, ContentManager Content, string Name)
        {
            ComponentOwner = Owner;
            this.Texture = Content.Load<Texture2D>(Name);
            Animator = new Animation2D();
            this.Type = Name;
        }

        public string Type
        {
            get
            {
                return this.Typename;
            }

            set
            {
                Typename = value;
            }
        }

        public Entity Owner
        {
            get
            {
                return ComponentOwner;
            }
        }

        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                Active = value;
            }
        }

        public bool Equals(IComponent other)
        {
            if (other == null) return false;
            return (this.Type.Equals(other.Type));
        }

        private string Typename;
        public Texture2D Texture;
        public Animation2D Animator;
        private Entity ComponentOwner;
        private bool Active;
    }
}
