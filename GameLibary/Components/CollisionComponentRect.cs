﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class CollisionComponentRect : IComponent
    {
        public CollisionComponentRect(string Name, Entity Owner, int X0,int X1, int Y0, int Y1)
        {
            ComponentOwner = Owner;
            rect.X = X0;
            rect.Width = X1;
            rect.Y = Y0;
            rect.Height = Y1;
            Active = true;
            Typename = Name;
        }

        public string Type
        {
            get
            {
                return Typename;
            }

            set
            {
                Typename = value;
            }
        }

        public Entity Owner
        {
            get
            {
                return ComponentOwner;
            }
        }

        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                Active = value;
            }
        }
        public bool Equals(IComponent other)
        {
            if (other == null) return false;
            return (this.Type.Equals(other.Type));
        }

        private string Typename;
        public Rectangle rect;
        private Entity ComponentOwner;
        private bool Active;

    }
}
