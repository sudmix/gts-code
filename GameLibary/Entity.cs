﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibary
{
    
    public class Entity 
    {
        public string Type
        {
            get { return this.Typename; }
            set { Typename = value; }
        }
       
        public bool IsActive
        {
            get { return this.Active; }
            set { this.Active = value; }
        }
        public Entity(string Name)
        {
            Typename = Name;
            this.Active = true;
            ComponentList = new List<IComponent>();
        }

        public void AddComponent(IComponent Component)
        {
           ComponentList.Add(Component);

        }

        public IComponent GetComponent(string Component)
        {
            if(ComponentList.Exists(item => item.Type == Component))
            {
                return ComponentList.Find(item => item.Type == Component);
            }

            return null;
        }

        public bool RemoveComponent(string Component)
        {
            return ComponentList.Remove(GetComponent(Component));
        }

        public void RemoveAll()
        {
            ComponentList.Clear();
        }




        public GameObject Parent;
        public GameObject Child;

        #region private member
        List<IComponent> ComponentList;
        private bool Active;
        private string Typename;
        #endregion
    }
}
