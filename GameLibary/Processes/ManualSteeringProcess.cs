﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameLibary
{
    public class ManualSteeringProcess : IProcess
    {
        public ManualSteeringProcess(bool Activate)
        {
            this.NodeList = new List<IProcessNode>();
            Type = "ManualSteeringProcess";
            this.Active = Activate;
        }
        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                this.Active = value;
            }
        }

        public string Type
        {
            get
            {
                return this.Typename;
            }

            set
            {
                this.Typename = value;
            }
        }
        public bool IsRenderable
        {
            get
            {
                return Renderable;
            }

            set
            {
                Renderable = value;
            }
        }
        public void Add(IProcessNode Node)
        {
            NodeList.Add(Node);
        }

        public bool Remove(IProcessNode Node)
        {
            return NodeList.Remove(Node);
        }



        public void End()
        {
            Active = false;
        }

        public bool Equals(IProcess other)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            Active = true;
        }

        public void Update(GameTime gameTime)
        {
           

            foreach (ManualSteeringNode Node in NodeList)
            {
                Node.ThePreviousGamePadState = Node.TheCurrentGamePadState;

                Node.ThePreviousKeyboardState = Node.TheCurrentKeyboardState;

                Node.TheCurrentKeyboardState = Keyboard.GetState();

                Node.TheCurrentGamePadState = GamePad.GetState(PlayerIndex.One);
                // Get Controls
                Node.Position.Value.X += Node.TheCurrentGamePadState.ThumbSticks.Left.X;

                Node.Position.Value.Y += Node.TheCurrentGamePadState.ThumbSticks.Left.Y;

                // Use the Keyboard / Dpad
                if (Node.TheCurrentKeyboardState.IsKeyDown(Keys.Left) || Node.TheCurrentGamePadState.DPad.Left == ButtonState.Pressed)
                {
                    Node.Position.Value.X -= Node.Velocity.Value.X;

                }

                if (Node.TheCurrentKeyboardState.IsKeyDown(Keys.Right) || Node.TheCurrentGamePadState.DPad.Right == ButtonState.Pressed)
                {
                    Node.Position.Value.X += Node.Velocity.Value.X;
                }

                if (Node.TheCurrentKeyboardState.IsKeyDown(Keys.Up) || Node.TheCurrentGamePadState.DPad.Up == ButtonState.Pressed)
                {
                    Node.Position.Value.Y -= Node.Velocity.Value.Y;
                }

                if (Node.TheCurrentKeyboardState.IsKeyDown(Keys.Down) || Node.TheCurrentGamePadState.DPad.Down == ButtonState.Pressed)
                {
                    Node.Position.Value.Y += Node.Velocity.Value.Y;
                }

                // Make sure that the player does not go out of bounds

              //  player.Position.X = MathHelper.Clamp(player.Position.X, 0, GraphicsDevice.Viewport.Width - player.Width);

              //   player.Position.Y = MathHelper.Clamp(player.Position.Y, 0, GraphicsDevice.Viewport.Height - player.Height);

                if (Node.TheCurrentKeyboardState.IsKeyDown(Keys.Space) || Node.TheCurrentGamePadState.Buttons.X == ButtonState.Pressed)
                {
                   
                }
            }
        }

        public void Render(SpriteBatch Batch)
        {
            throw new NotImplementedException();
        }


        private List<IProcessNode> NodeList;
        private bool Active;
        private string Typename;
        private bool Renderable;
    }
}
