﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class RenderProcess2D : IProcess
    {
        public RenderProcess2D(bool Activate)
        {
            Renderable = true;
            NodeList = new List<IProcessNode>();
            this.Active = Activate;
            Type = "RenderProcess2D";
        }

        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                this.Active = value;
            }
        }

        public string Type
        {
            get
            {
                return this.Typename;
            }

            set
            {
                this.Typename = value;
            }
        }
        public bool IsRenderable
        {
            get
            {
                return Renderable;
            }

            set
            {
                Renderable = value;
            }
        }
        public void Add(IProcessNode Node)
        {
            NodeList.Add(Node);
        }

        public bool Remove(IProcessNode Node)
        {
            return NodeList.Remove(Node);
        }



        public void End()
        {
            Active = false;
        }

        public bool Equals(IProcess other)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            Active = true;
        }

        public void Update(GameTime gameTime)
        {
            // do nothing
        }

        public void Render(SpriteBatch Batch)
        {
            if(Active)
            {
                foreach(RenderNode2D Node in NodeList)
                {
                    Batch.Draw(Node.Renderer.Texture, Node.Position.Value, Color.White);
                }
            }
          
        }
        private List<IProcessNode> NodeList;
        private bool Active;
        private string Typename;
        private bool Renderable;

    }
}
