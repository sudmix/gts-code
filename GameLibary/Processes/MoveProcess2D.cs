﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class MoveProcess2D : IProcess
    {
        public MoveProcess2D(bool Activate)
        {
            NodeList = new List<IProcessNode>();

            Active = Activate;

            Type = "MoveProcess2D";

            Renderable = false;
        }

        public void Add(IProcessNode Node)
        {
            NodeList.Add(Node);
        }

        public bool Remove(IProcessNode Node)
        {
            return NodeList.Remove(Node);
        }


        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                Active = value;
            }
        }

        string IProcess.Type
        {
            get
            {
                return Type;
            }

            set
            {
                Type = value;
            }
        }

        public bool IsRenderable
        {
            get
            {
                return Renderable;
            }

            set
            {
                Renderable = value;
            }
        }

        public void End()
        {
            Active = false;
        }

        bool IEquatable<IProcess>.Equals(IProcess other)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            this.Active = true;
        }

        public void Render(SpriteBatch Batch)
        {
            throw new NotImplementedException();
        }

        public void Update(GameTime gameTime)
        {
            if(Active)
            {               
                foreach (MoveNode2D ProcessNode in NodeList)
                {
                    // Nur wenn alles aktiv, dann verändern
                    if(ProcessNode.Position.IsActive && ProcessNode.Velocity.IsActive)
                        ProcessNode.Position.Value += ProcessNode.Velocity.Value * (float)(((gameTime.ElapsedGameTime.Milliseconds))*0.001);
                }
            }
           
        }

        private List<IProcessNode> NodeList;
        private bool Active;
        private string Type;
        private bool Renderable;
    }
}
