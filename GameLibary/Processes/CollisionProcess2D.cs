﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class CollisionProcess2D : IProcess
    {
        public CollisionProcess2D(GraphicsDevice gfxDevice, bool Activate, bool Draw = false)
        {
            this.DrawRect = Draw;
            this.NodeList = new List<IProcessNode>();
            Type = "CollisionProcess2D";
            Active = Activate;

            if (DrawRect)
                Renderable = true;
            else
                Renderable = false;

            FirstRectangle = new DrawRectangle();
            FirstRectangle.Initialize(gfxDevice);
            OtherRectangle = new DrawRectangle();
            OtherRectangle.Initialize(gfxDevice);
        }
        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                this.Active = value;
            }
        }

        public string Type
        {
            get
            {
                return this.Typename;
            }

            set
            {
                this.Typename = value;
            }
        }
        public bool IsRenderable
        {
            get
            {
                return Renderable;
            }

            set
            {
                Renderable = value;
            }
        }
        public void Add(IProcessNode Node)
        {
            NodeList.Add(Node);
        }

        public bool Remove(IProcessNode Node)
        {
            return NodeList.Remove(Node);
        }



        public void End()
        {
            Active = false;
        }

        public bool Equals(IProcess other)
        {
            if (other == null) return false;
            return (this.GetType().Equals(other.GetType()));
        }

        public void Start()
        {
            Active = true;
        }

        public void Render(SpriteBatch Batch)
        {
            FirstRectangle.Draw(Batch, 1, Color.Red);
            OtherRectangle.Draw(Batch, 2, Color.Green);
        }

        public void Update(GameTime gameTime)
        {
            foreach(ColliderNode2D FirstNode in NodeList)
            {

                FirstNode.TheRect.rect.X = (int)FirstNode.ThePosition.Value.X - FirstNode.TheRect.rect.Width /2;
                FirstNode.TheRect.rect.Y = (int)FirstNode.ThePosition.Value.Y - FirstNode.TheRect.rect.Height / 2;

                FirstRectangle.Update(FirstNode.TheRect.rect);

                foreach (ColliderNode2D OtherNode in NodeList)
                {

                    if (FirstNode.TheCollider.DoIgnore(OtherNode.TheCollider.OwnerName))
                        continue;
                    

                    // Wenn der gleiche, dann nicht prüfen.
                    if (FirstNode!=OtherNode)
                    {
                        OtherNode.TheRect.rect.X = (int)OtherNode.ThePosition.Value.X - OtherNode.TheRect.rect.Width / 2; 
                        OtherNode.TheRect.rect.Y = (int)OtherNode.ThePosition.Value.Y - OtherNode.TheRect.rect.Height / 2; ;
                        OtherRectangle.Update(OtherNode.TheRect.rect);

                   
                    
                        // Kollision erkannt
                        if((FirstNode.TheRect.rect.Intersects(OtherNode.TheRect.rect)))
                        {
                            // Kollisionsfunktion ausführen
                            if((FirstNode.TheRect.IsActive) && (OtherNode.TheRect.IsActive))
                                 FirstNode.TheCollider.OnEnterTrigger(OtherNode.TheRect.Owner);
                        }
                        else if((FirstNode.TheCollider.CollInfo.IsCollide))
                        {
                          //  if ((FirstNode.TheRect.IsActive) && (/*OtherNode.TheRect.IsActive*/1==1))
                                FirstNode.TheCollider.OnExitTrigger(FirstNode.TheCollider.CollInfo.Other);
                        }
                    }
                   
                    
                }
            }
        }

        private bool DrawRect;
        private bool Active;
        private bool Renderable;
        private string Typename;
        private List<IProcessNode> NodeList;
        public DrawRectangle FirstRectangle;
        public DrawRectangle OtherRectangle;
    }
}
