﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class AnimationProcess2D : IProcess
    {
        public AnimationProcess2D(bool Activate)
        {
            Renderable = true;
            NodeList = new List<IProcessNode>();
            this.Active = Activate;
            Type = "AnimationProcess2D";
        }

        public bool IsActive
        {
            get
            {
                return this.Active;
            }

            set
            {
                Active = value;
            }
        }

        public bool IsRenderable
        {
            get
            {
                return this.Renderable;
            }

            set
            {
                this.Renderable = value;
            }
        }

        public string Type
        {
            get
            {
                return Typename;
            }

            set
            {
                Typename = value;
            }
        }

        public void Add(IProcessNode Node)
        {
            NodeList.Add(Node);
         
        }

        public void End()
        {
            this.Active = false;
        }

        public bool Equals(IProcess other)
        {
            if (other == null) return false;
            return (this.GetType().Equals(other.GetType()));
        }

        public bool Remove(IProcessNode Node)
        {
            return NodeList.Remove(Node);
        }

        public void Render(SpriteBatch Batch)
        {
            foreach(AnimationNode2D Node in NodeList)
            {
                if(Node.Animation.IsActive)
                    Node.Animation.Animator.Draw(Batch);
            }
        }

        public void Start()
        {
            Active = true;
        }

        public void Update(GameTime gameTime)
        {
            foreach (AnimationNode2D Node in NodeList)
            {
                if (Node.Animation.IsActive)
                    Node.Animation.Animator.Update(gameTime);
            }
        }

        private List<IProcessNode> NodeList;
        private bool Active;
        private string Typename;
        private bool Renderable;
    }
}
