﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public interface IProcess: IEquatable<IProcess>
    {
        bool IsActive { get; set; }
        string Type { get; set; }
        void Add(IProcessNode Node);
        bool Remove(IProcessNode Node);    
        void Start();
        void Update(GameTime gameTime);
        void Render(SpriteBatch Batch);
        void End();

        bool IsRenderable { get; set;}

    }
}

