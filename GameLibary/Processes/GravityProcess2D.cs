﻿/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class GravityProcess2D : IProcess
    {
        public GravityProcess2D(bool Activate)
        {
            this.NodeList = new List<IProcessNode>();
            Type = "GravityProcess2D";
            this.Active = Activate;
        }
        public bool IsActive
        {
            get
            {
                return Active;
            }

            set
            {
                this.Active = value;
            }
        }

        public string Type
        {
            get
            {
                return this.Typename;
            }

            set
            {
                this.Typename = value;
            }
        }
        public bool IsRenderable
        {
            get
            {
                return Renderable;
            }

            set
            {
                Renderable = value;
            }
        }
        public void Add(IProcessNode Node)
        {
            NodeList.Add(Node);
        }

        public bool Remove(IProcessNode Node)
        {
            return NodeList.Remove(Node);
        }



        public void End()
        {
            Active = false;
        }

        public bool Equals(IProcess other)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            Active = true;
        }

        public void Update(GameTime gameTime)
        {
            foreach(GravityNode2D Node in NodeList)
            {
                if(Node.Gravity.IsActive)
                     Node.Position.Value.Y += (float)Node.Gravity.Value.Y * ((float)0.5 *  (float)(gameTime.ElapsedGameTime.Milliseconds *0.01) * (float)(gameTime.ElapsedGameTime.Milliseconds * 0.01));
            }
        }

        public void Render(SpriteBatch Batch)
        {
            throw new NotImplementedException();
        }

       
        private List<IProcessNode> NodeList;
        private bool Active;
        private string Typename;
        private bool Renderable;
       

    }
}
