﻿
/****************************** Module Header ******************************\

Copyright (c)2017 Marcus Finzel

See http://www.sudmix.de
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibary
{
    public class ProcessManager
    {

        public ProcessManager()
        {
            this.ProcessList = new List<IProcess>();
            this.RenderProcessList = new List<IProcess>();
        }

        ~ProcessManager()
        {
            throw new System.NotImplementedException();
        }

        public void AddProcess(IProcess Process)
        {
            if (Process.IsRenderable)
                this.RenderProcessList.Add(Process);
            else
                 this.ProcessList.Add(Process);

        }

        public bool RemoveProcess(IProcess Process)
        {
            return this.ProcessList.Remove(Process);
        }

        public IProcess GetProcess(string ProcessType)
        {
            if (ProcessList.Exists(item => item.Type == ProcessType))
            {
                return ProcessList.Find(item => item.Type == ProcessType);
            }
            else if (RenderProcessList.Exists(item => item.Type == ProcessType))
            {
                return RenderProcessList.Find(item => item.Type == ProcessType);
            }

            return null;
        }

        public void Update(GameTime gameTime)
        {
            foreach(IProcess aProcess in ProcessList)
            {
                if(aProcess.IsActive)
                    aProcess.Update(gameTime);
            }
            foreach (IProcess aProcess in RenderProcessList)
            {
                if (aProcess.IsActive)
                    aProcess.Update(gameTime);
            }
        }

        public void Render(SpriteBatch Batch)
        {
            foreach (IProcess aProcess in RenderProcessList)
            {
                if (aProcess.IsActive)
                    aProcess.Render(Batch);
            }
        }

        #region private member
        List<IProcess> ProcessList;
        List<IProcess> RenderProcessList;
        #endregion
    }
}
